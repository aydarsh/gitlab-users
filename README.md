Simple Flask service that retrieves a GitLab project users  

To start the application go to the `gitlab-users` directory and type commands as shown below:  
    
    export FLASK_APP=start.py
    flask run --host=0.0.0.0

Open a REST client and send a `GET` request to `/users` endpoint with the following header:

    GitLab-Private-Token: <private token>
    GitLab-Project-Id: <project id>
    GitLab-Host: <https://gitlab.com>
