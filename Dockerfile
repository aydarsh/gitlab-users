FROM python:3.6-alpine

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

COPY . /usr/src/app

EXPOSE 5000
ENV FLASK_APP start.py
ENTRYPOINT ["flask", "run", "--host=0.0.0.0"]