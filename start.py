import datetime
import requests
import json
from flask import Flask, jsonify, request

app = Flask(__name__)
    
@app.route('/users', methods=['GET'])
def users():
    gitlab_host = request.headers['GitLab-Host'] if 'GitLab-Host' in request.headers else 'https://gitlab.com'
    gitlab_token = request.headers['GitLab-Private-Token'] if 'GitLab-Private-Token' in request.headers else None
    gitlab_project_id = request.headers['GitLab-Project-Id'] if 'GitLab-Project-Id' in request.headers else None
    header = {'Content-Type': 'application/json', 'Private-Token': f'{gitlab_token}'}
    api_url = f'{gitlab_host}/api/v4/projects/{gitlab_project_id}/users'
    response = requests.get(api_url, headers=header)

    return jsonify(json.loads(response.content.decode('utf-8')))
    
app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
